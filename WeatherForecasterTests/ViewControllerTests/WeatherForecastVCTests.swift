//
//  WeatherForecastVCTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 24/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

extension WeatherForecastVCTests {
    class MockWeatherForecastViewModel: WeatherForecastViewModel {
        func numberOfDailyForecasts() -> Int {
            return 0
        }
        
        var loadWeatherForecastCalled = false
        func loadWeatherForecast(for locationId: Int) {
            self.loadWeatherForecastCalled = true
        }
        
        func hourlyForecast(at dailyForecastIndex: Int, hourlyForecastIndex: Int) -> HourlyForecastViewModel? {
            return nil
        }
        
        func numberOfHourlyForecasts(at dailyForecastIndex: Int) -> Int {
            return 0
        }
        
        func titleOfDailyForecast(at dailyForecastIndex: Int) -> String {
            return ""
        }
        
        func locationName() -> String {
            return ""
        }
    }
}

class WeatherForecastVCTests: XCTestCase {
    var sut: WeatherForecastVC!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "WeatherForecastVC") as! WeatherForecastVC
    }
    
    func testForecastTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.forecastTableView)
    }
    
    func testForecastTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.forecastTableViewDataSource = ForecastTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.forecastTableView.dataSource is ForecastTableViewDataSource)
    }
    
    func testLoadWeatherForecast_whenViewIsLoaded_shouldAskViewModelForWeatherForecast() {
        // Arrange
        let mockViewModel = MockWeatherForecastViewModel()
        sut.viewModel = mockViewModel
        //Act
        _ = sut.view
        // Assert
        XCTAssert(mockViewModel.loadWeatherForecastCalled)
    }
}
