//
//  WeatherForecastViewModelTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

extension WeatherForecastViewModelTests {
    class MockWeatherManager: WeatherManager {
        var locationId: Int!
        var completionHandler: ((WeatherForecast?, WeatherManagerError?) -> Void)?
        
    func weatherForecast(for locationId: Int, completionHandler: @escaping (_ weatherForecast: WeatherForecast?, _ error: WeatherManagerError?) -> Void) {
            self.locationId = locationId
            self.completionHandler = completionHandler
        }
    }
    
    class MockWeatherForecastViewModelViewDelegate: WeatherForecastViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var weatherForecastResult: WeatherForecastViewModelResult?
        
        func weatherForecastLoaded(with result: WeatherForecastViewModelResult) {
            guard let asynExpectation = expectation else {
                XCTFail("MockWeatherForecastViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.weatherForecastResult = result
            asynExpectation.fulfill()
        }
    }
}

class WeatherForecastViewModelTests: XCTestCase {
    
    var sut: WFWeatherForecastViewModel!
    var mockWeatherManager: MockWeatherManager!
    var mockViewDelegate: MockWeatherForecastViewModelViewDelegate!
    
    
    override func setUp() {
        super.setUp()
        mockWeatherManager = MockWeatherManager()
        mockViewDelegate = MockWeatherForecastViewModelViewDelegate()
        sut = WFWeatherForecastViewModel(weatherManager: mockWeatherManager, viewDelegate: mockViewDelegate)
    }
    
    func testLoadWeatherForecastForLocationId_shouldAskWeatherManagerForForecastOfLocation() {
        // Act
        sut.loadWeatherForecast(for: 123)
        // Assert
        XCTAssertEqual(mockWeatherManager.locationId, 123)
    }
    
    func testLoadWeatherForecastForLocationId_whenForecastFound_raisesWeatherForecastLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadWeatherForecast(for: 123)
        mockWeatherManager.completionHandler?(WFWeatherForecast(), nil)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.weatherForecastResult! == .success)
        }
    }
    
    func testLoadWeatherForecastForLocationId_whenNoForecastFound_raisesWeatherForecastLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadWeatherForecast(for: 123)
        mockWeatherManager.completionHandler?(nil, WeatherManagerError.noForecastFoundError)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.weatherForecastResult! == .failure)
        }
    }
    
    func testNoOfDailyForecasts_whenNoForecastsFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadWeatherForecast(for: 123)
        mockWeatherManager.completionHandler?(nil, WeatherManagerError.noForecastFoundError)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfDailyForecasts(), 0)
        }
    }
    
    func testNoOfDailyForecasts_whenOneForecastFound_returnsOne() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadWeatherForecast(for: 123)
        let weatherForecast = WFWeatherForecast(dailyForecasts: [WFDailyForecast()], location: nil)
        mockWeatherManager.completionHandler?(weatherForecast, nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfDailyForecasts(), 1)
        }
    }
    
    func testNoOfDailyForecasts_whenFiveForecastsFound_returnsFive() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadWeatherForecast(for: 123)
        let weatherForecast = WFWeatherForecast(dailyForecasts: [WFDailyForecast(), WFDailyForecast(), WFDailyForecast(), WFDailyForecast(), WFDailyForecast()], location: nil)
        mockWeatherManager.completionHandler?(weatherForecast, nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfDailyForecasts(), 5)
        }
    }
}
