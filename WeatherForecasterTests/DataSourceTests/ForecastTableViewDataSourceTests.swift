//
//  ForecastTableViewDataSourceTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 24/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

extension ForecastTableViewDataSourceTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockHourlyForecastTableViewCell: UITableViewCell, HourlyForecastTableViewCell {
        var configCellGotCalled = false
        var hourlyForecast : HourlyForecastViewModel?
        
        func configCell(with hourlyForecast: HourlyForecastViewModel) {
            configCellGotCalled = true
            self.hourlyForecast = hourlyForecast
        }
    }
    
    class MockWeatherForecastViewModel: WeatherForecastViewModel {
        var dailyForecasts: [DailyForecast]?
        
        func loadWeatherForecast(for locationId: Int) { }
        func hourlyForecast(at dailyForecastIndex: Int, hourlyForecastIndex: Int) -> HourlyForecastViewModel? {
            guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
                let hourlyForecasts = dailyForecasts[dailyForecastIndex].hourlyForecasts, hourlyForecastIndex < hourlyForecasts.count else {
                    return nil
            }
            let hourlyForecast = hourlyForecasts[hourlyForecastIndex]
            return WFHourlyForecastViewModel(hourlyForecast: hourlyForecast)
        }
        
        func numberOfDailyForecasts() -> Int {
            guard let dailyForecasts = dailyForecasts else {
                return 0
            }
            return dailyForecasts.count
        }
        
        func numberOfHourlyForecasts(at dailyForecastIndex: Int) -> Int {
            guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
                let hourlyForecasts = dailyForecasts[dailyForecastIndex].hourlyForecasts else {
                    return 0
            }
            return hourlyForecasts.count
        }
        
        func titleOfDailyForecast(at dailyForecastIndex: Int) -> String {
            guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
                let date = dailyForecasts[dailyForecastIndex].date else {
                    return ""
            }
            return date.dayOfWeek()
        }
        
        func locationName() -> String {
            return ""
        }
    }
}

class ForecastTableViewDataSourceTests: XCTestCase {
    
    var sut : ForecastTableViewDataSource!
    var viewModel: MockWeatherForecastViewModel!
    
    override func setUp() {
        super.setUp()
        sut = ForecastTableViewDataSource()
        viewModel =  MockWeatherForecastViewModel()
        sut.viewModel = viewModel
    }
    
    func testDataSourceHasWeatherForecastViewModel() {
        // Arrange
        sut.viewModel = viewModel
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is WeatherForecastViewModel)
    }
    
    func testNumberOfSections_WithTwoDailyForecasts_ShouldReturnTwo() {
        //Arrange
        let dailyForecasts = [WFDailyForecast(), WFDailyForecast()]
        viewModel.dailyForecasts = dailyForecasts
        let mockTableView = MockTableView()
        
        //Act
        let noOfSections = sut.numberOfSections(in: mockTableView)
        
        //Assert
        XCTAssertEqual(noOfSections, 2, "Number of section should be 2")
    }
    
    func testTitleForHeaderInSection_WithThreeDailyForecasts_ShouldReturnTitleOfEachForecastAsSectionHeaderTitle() {
        //Arrange
        let dateZero = Date()
        let dateOne = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        let dateTwo = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
        
        let dailyForecasts = [WFDailyForecast(date: dateZero), WFDailyForecast(date: dateOne), WFDailyForecast(date: dateTwo)]
        viewModel.dailyForecasts = dailyForecasts
        let mockTableView = MockTableView()
        
        //Act
        let sectionZeroTitle = sut.tableView(mockTableView, titleForHeaderInSection: 0)
        let sectionOneTitle = sut.tableView(mockTableView, titleForHeaderInSection: 1)
        let sectionTwoTitle = sut.tableView(mockTableView, titleForHeaderInSection: 2)
        
        //Assert
        XCTAssertEqual(sectionZeroTitle!, dateZero.dayOfWeek())
        XCTAssertEqual(sectionOneTitle!, dateOne.dayOfWeek())
        XCTAssertEqual(sectionTwoTitle!, dateTwo.dayOfWeek())
    }
    
    func testNumberOfRowsInSection_WithFourHourlyForecastsInADailyForecast_ShouldReturnFour() {
        //Arrange
        let dailyForecast = WFDailyForecast(hourlyForecasts: [WFHourlyForecast(), WFHourlyForecast(), WFHourlyForecast(), WFHourlyForecast()])
        viewModel.dailyForecasts = [dailyForecast]
        let mockTableView = MockTableView()
        
        //Act
        let noOfRowsInSectionZero = sut.tableView(mockTableView, numberOfRowsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfRowsInSectionZero, 4, "Number of rows in section 0 should be 2")
    }
    
    func testCellForRow_ReturnsWeatherForecastTableViewCell() {
        //Arrange
        let dailyForecasts = [WFDailyForecast(hourlyForecasts: [WFHourlyForecast()])]
        viewModel.dailyForecasts = dailyForecasts
        
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockHourlyForecastTableViewCell.self, forCellReuseIdentifier: "HourlyForecastTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is HourlyForecastTableViewCell,"Returned cell is of HourlyForecastTableViewCell type")
    }
    
    func testCellForRow_DequeuesCell() {
        //Arrange
        let dailyForecasts = [WFDailyForecast(hourlyForecasts: [WFHourlyForecast()])]
        viewModel.dailyForecasts = dailyForecasts
        
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockHourlyForecastTableViewCell.self, forCellReuseIdentifier: "HourlyForecastTableViewCell")
        
        //Act
        _ = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockTableView.cellGotDequeued,"CellForRow should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForRow() {
        //Arrange
        let dailyForecasts = [WFDailyForecast(hourlyForecasts: [WFHourlyForecast()])]
        viewModel.dailyForecasts = dailyForecasts
        
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockHourlyForecastTableViewCell.self, forCellReuseIdentifier: "HourlyForecastTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0)) as! MockHourlyForecastTableViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForRow should be calling ConfigCell method")
    }
}
