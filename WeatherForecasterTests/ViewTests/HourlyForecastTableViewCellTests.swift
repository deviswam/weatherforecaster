//
//  HourlyForecastTableViewCellTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 27/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

//MARK: MOCKS & FAKES
extension HourlyForecastTableViewCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return WFHourlyForecastTableViewCell()
        }
    }
    
    class FakeHourlyForecastViewModel: HourlyForecastViewModel {
        var hour: String { return "20" }
        var temp: String { return "10°C" }
        var minTemp: String { return "7"}
        var maxTemp: String { return "12" }
        var condition: String { return "Sunny" }
    }
}

class HourlyForecastTableViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var tableView : UITableView!
    var sut : WFHourlyForecastTableViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let weatherForecastVC = storyBoard.instantiateViewController(withIdentifier: "WeatherForecastVC") as! WeatherForecastVC
        weatherForecastVC.forecastTableViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = weatherForecastVC
        _ = weatherForecastVC.view
        tableView = weatherForecastVC.forecastTableView
        sut = tableView.dequeueReusableCell(withIdentifier: "HourlyForecastTableViewCell", for: IndexPath(row: 0, section: 0)) as! WFHourlyForecastTableViewCell
    }
    
    func testHourlyForecastTableViewCell_HasHourLabel() {
        // Assert
        XCTAssertNotNil(sut.hourLabel)
    }
    
    func testHourlyForecastTableViewCell_HasConditionLabel() {
        // Assert
        XCTAssertNotNil(sut.conditionLabel)
    }
    
    func testHourlyForecastTableViewCell_HasTempLabel() {
        // Assert
        XCTAssertNotNil(sut.tempLabel)
    }
    
    func testHourlyForecastTableViewCell_HasMinTempLabel() {
        // Assert
        XCTAssertNotNil(sut.minTempLabel)
    }
    
    func testHourlyForecastTableViewCell_HasMaxTempLabel() {
        // Assert
        XCTAssertNotNil(sut.maxTempLabel)
    }
    
    func testConfigCell_withHourlyForecastViewModel_ShouldSetTextOfEachLabel() {
        //Act
        sut.configCell(with: FakeHourlyForecastViewModel())
        
        //Assert
        XCTAssertEqual(sut.hourLabel.text!, "20")
        XCTAssertEqual(sut.conditionLabel.text!, "Sunny")
        XCTAssertEqual(sut.tempLabel.text!, "10°C")
        XCTAssertEqual(sut.minTempLabel.text!, "7")
        XCTAssertEqual(sut.maxTempLabel.text!, "12")
    }
}
