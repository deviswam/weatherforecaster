//
//  WeatherAPITests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

extension WeatherAPITests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class WeatherAPITests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: WFWeatherAPI!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = WFWeatherAPI(urlSession: mockURLSession)
    }
    
    func testConformanceToWeatherAPIProtocol() {
        XCTAssertTrue((sut as AnyObject) is WeatherAPI)
    }
    
    func testfiveDaysWeatherForecast_ShouldAskURLSessionForFetchingForecastWeatherData() {
        // Act
        sut.fiveDaysWeatherForecast(for: 123) { (weatherForecast, error) in }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testfiveDaysWeatherForecast_WithValidJSONOfLocation_PopulatesLocationInReturnedForecastObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"city\":{" +
            "\"id\":524901," +
            "\"name\":\"London\"," +
            "\"country\":\"GB\"" +
            "}," +
            "\"list\":[{}]" +
            "}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        let expectedLocationId = 524901
        var receivedLocation : Location?
        var receivedError : WeatherAPIError?
        
        //Act
        sut.fiveDaysWeatherForecast(for: expectedLocationId) { (weatherForecast: WeatherForecast?, error: WeatherAPIError?) in
            if let location = weatherForecast?.location {
                receivedLocation = location
            }
            receivedError = error
            asynExpectation.fulfill()
        }

        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedLocation?.id == expectedLocationId, "Fetched and expected location should have same id")
            XCTAssertTrue(receivedLocation?.name == "London", "Fetched and expected location should have same name")
            XCTAssertTrue(receivedLocation?.country == "GB", "Fetched and expected location should have same country")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with Location object")
        }
    }
    
    func testfiveDaysWeatherForecast_WithValidJSONOfAForecast_PopulatesADailyForecastInReturnedForecastObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"city\":{}," +
            "\"list\":[{" +
                "\"dt\": 1492959600," +
                "\"main\": {" +
                    "\"temp\": 15.53," +
                    "\"temp_min\": 15.06," +
                    "\"temp_max\": 15.53" +
                "}," +
                "\"weather\": [{" +
                    "\"main\": \"Clear\"" +
                "}]" +
            "}]}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        let locationId = 524901
        var receivedDailyForecast : DailyForecast?
        var receivedError : WeatherAPIError?
        let expectedDate = Date(timeIntervalSince1970: 1492959600)
        
        //Act
        sut.fiveDaysWeatherForecast(for: locationId) { (weatherForecast: WeatherForecast?, error: WeatherAPIError?) in
            if let dailyForecast = weatherForecast?.dailyForecasts?.first {
                receivedDailyForecast = dailyForecast
            }
            receivedError = error
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertNil(receivedError)
            XCTAssertTrue(receivedDailyForecast?.hourlyForecasts?.first?.date == expectedDate)
            XCTAssertTrue(receivedDailyForecast?.hourlyForecasts?.first?.temperature == 16)
            XCTAssertTrue(receivedDailyForecast?.hourlyForecasts?.first?.minTemperature == 15)
            XCTAssertTrue(receivedDailyForecast?.hourlyForecasts?.first?.maxTemperature == 16)
            XCTAssertEqual(receivedDailyForecast?.hourlyForecasts?.first?.condition, "Clear")
        }
    }    
}
