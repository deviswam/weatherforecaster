//
//  WeatherManagerTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 23/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

extension WeatherManagerTests {
    class MockWeatherAPI: WeatherAPI {
        var fiveDaysWeatherForecastCalled = false
        var completionHandler: ((WeatherForecast?, WeatherAPIError?) -> Void)?
        func fiveDaysWeatherForecast(for locationId: Int, completionHandler: @escaping (WeatherForecast?, WeatherAPIError?) -> Void) {
            self.fiveDaysWeatherForecastCalled = true
            self.completionHandler = completionHandler
        }
    }
}

class WeatherManagerTests: XCTestCase {
    var mockWeatherAPI: MockWeatherAPI!
    var sut: WFWeatherManager!
    
    override func setUp() {
        super.setUp()
        mockWeatherAPI = MockWeatherAPI()
        sut = WFWeatherManager(weatherAPI: mockWeatherAPI)
    }
    
    func testConformanceToWeatherManagerProtocol() {
        //Assert
        XCTAssertTrue((sut as AnyObject) is WeatherManager, "WMWeatherManager should conforms to WeatherManager protocol")
    }
    
    func testWeatherForecast_withLocationId_asksWeatherAPIToFetchWeatherForecast() {
        // Act
        sut.weatherForecast(for: 123) { (weatherForecast, error) in
            
        }
        // Assert
        XCTAssert(mockWeatherAPI.fiveDaysWeatherForecastCalled)
    }
    
    func testWeatherForecast_shouldReceiveWeatherForecastWithNoErrorWhenForecastFound() {
        // Arrange
        var receivedWeatherForecast : WeatherForecast?
        var receivedError : WeatherManagerError?
        let expectedWeatherForecast = WFWeatherForecast()
        
        // Act
        sut.weatherForecast(for: 123) { (weatherForecast, error) in
            receivedWeatherForecast = weatherForecast
            receivedError = error
        }
        mockWeatherAPI.completionHandler?(expectedWeatherForecast, nil)
        
        // Assert
        XCTAssertNotNil(receivedWeatherForecast)
        XCTAssertNil(receivedError)
    }
}
