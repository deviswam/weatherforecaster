//
//  LocationTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

class WMLocationTests: XCTestCase {
    func testConformanceToLocationProtocol() {
        // Arrange
        let sut = WFLocation(id: 0)
        // Assert
        XCTAssert((sut as Any) is Location)
    }
    
    func testInit_LocationMustBeCreatedWithAnId() {
        // Arrange
        let locationId = 123
        let sut = WFLocation(id: locationId)
        // Assert
        XCTAssertEqual(sut.id, locationId)
    }
    
    func testLocationShouldHaveAName() {
        // Arrange
        let locationId = 123
        let name = "London"
        let sut = WFLocation(id: locationId, name: name)
        // Assert
        XCTAssertEqual(sut.name, name)
    }
    
    func testLocationShouldHaveACountry() {
        // Arrange
        let locationId = 123
        let country = "GB"
        let sut = WFLocation(id: locationId, country: country)
        // Assert
        XCTAssertEqual(sut.country, country)
    }
    
    func testLocationsForEquality_WithSameId_AreSameLocations() {
        //Arrange
        let location1 = WFLocation(id: 123, name: "London")
        let location2 = WFLocation(id: 123)
        
        //Assert
        XCTAssert(location1 == location2, "Both are same locations")
    }
    
    func testLocationConformsToObjectMapperProtocol() {
        //Arrange
        let location = WFLocation(id: 123)
        
        //Assert
        XCTAssertTrue((location as Any) is ObjectMapper, "Location should conforms to ObjectMapper protocol")
    }
    
    func testInit_WithNilDictionary_ShouldFailInitialization() {
        //Arrange
        let location = WFLocation(dictionary: nil)
        
        //Assert
        XCTAssertNil(location, "Location should be created only with valid json dictionary")
    }
    
    func testInit_WithNoLocationIdInDictionary_ShouldFailInitialization() {
        //Arrange
        let jsonLocationDictionary = ["name":"London"]
        let location = WFLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        //Assert
        XCTAssertNil(location, "Location should only be created if dictionary has location Id")
    }
    
    func testInit_WithNoLocationNameInDictionary_ShouldNotFailInitialization() {
        //Arrange
        let jsonLocationDictionary = ["id":123]
        let location = WFLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertNotNil(location, "Only Location id is mandatory to create Location object")
    }
    
    func testInit_WithLocationIdAndNameInDictionary_ShouldSetLocationIdAndName() {
        //Arrange
        let jsonLocationDictionary = ["id":123, "name":"London"] as [String : Any]
        let location = WFLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertEqual(location?.id, 123)
        XCTAssertEqual(location?.name, "London")
    }
    
}
