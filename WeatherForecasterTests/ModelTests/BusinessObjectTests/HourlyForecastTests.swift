//
//  HourlyForecastTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 23/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

class HourlyForecastTests: XCTestCase {
    func testConformanceToHourlyForecastProtocol() {
        // Arrange
        let sut = WFHourlyForecast()
        // Assert
        XCTAssert((sut as Any) is HourlyForecast)
    }
    
    func testHourlyForecastShouldHaveADate() {
        //Arrange
        let currentDateTime = Date()
        let sut = WFHourlyForecast(date: currentDateTime)
        
        //Assert
        XCTAssertEqual(sut.date, currentDateTime, "Hourly Forecast should have a date")
    }
    
    func testTemperatureProperty_WithDecimalValueLessThanHalf_RoundsDown() {
        //Arrange
        let temp = 5.3
        let sut = WFHourlyForecast(temp: temp)
        
        //Assert
        XCTAssertEqual(sut.temperature, 5)
    }
    
    func testTemperatureProperty_WithDecimalValueEqualToHalf_RoundsUp() {
        //Arrange
        let temp = 5.5
        let sut = WFHourlyForecast(temp: temp)
        
        //Assert
        XCTAssertEqual(sut.temperature, 6)
    }
    
    func testTemperatureProperty_WithDecimalValueGreaterThanHalf_RoundsUp() {
        //Arrange
        let temp = 5.7
        let sut = WFHourlyForecast(temp: temp)
        
        //Assert
        XCTAssertEqual(sut.temperature, 6)
    }
    
    func testHourlyForecastShouldHaveAMinTemp() {
        //Arrange
        let minTemp = 5.3
        let sut = WFHourlyForecast(minTemp: minTemp)
        
        //Assert
        XCTAssertEqual(sut.minTemperature, 5)
    }
    
    func testHourlyForecastShouldHaveAMaxTemp() {
        //Arrange
        let maxTemp = 5.3
        let sut = WFHourlyForecast(maxTemp: maxTemp)
        
        //Assert
        XCTAssertEqual(sut.maxTemperature, 5)
    }
    
    func testHourlyForecastShouldHaveACondition() {
        //Arrange
        let condition = "Cloudy"
        let sut = WFHourlyForecast(condition: condition)
        
        //Assert
        XCTAssertEqual(sut.condition, condition)
    }
    
    func testHourlyForecastConformsToObjectMapperProtocol() {
        //Arrange
        let sut = WFHourlyForecast()
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "Hourly Forecast should conforms to ObjectMapper protocol")
    }
    
    func testInit_WithNilDictionary_ShouldFailInitialization() {
        //Arrange
        let sut = WFHourlyForecast(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "Hourly Forecast should be created only with valid json dictionary")
    }
    
    func testInit_WithValidDictionaryOfHourlyForecast_ShouldSetAllFieldsWithCorrectTransformation() {
        //Arrange
        let timeInterval: Double = 1489748400
        let expectedDate = Date(timeIntervalSince1970: timeInterval)
        let mainDictionary = ["temp": 5.33, "temp_min": 4.5, "temp_max": 9.6]
        let weatherArray = [["main": "Cloudy"]]
        
        //Act
        let hourlyForecastDictionary = ["dt": timeInterval, "main": mainDictionary, "weather": weatherArray] as [String : Any]
        let sut = WFHourlyForecast(dictionary: hourlyForecastDictionary)
        
        //Assert
        XCTAssertEqual(sut?.date, expectedDate, "returned and expected dates should be same")
        XCTAssertEqual(sut?.temperature, 5)
        XCTAssertEqual(sut?.minTemperature, 5)
        XCTAssertEqual(sut?.maxTemperature, 10)
        XCTAssertEqual(sut?.condition, "Cloudy")
    }
}
