//
//  WFWeatherForecastTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

class WeatherForecastTests: XCTestCase {
    func testConformanceToWeatherForecastProtocol() {
        // Arrange
        let sut = WFWeatherForecast()
        // Assert
        XCTAssert((sut as Any) is WeatherForecast)
    }
    
    func testWeatherForecastShouldHaveALocation() {
        // Arrange
        let expectedLocation = WFLocation(id: 123)
        let sut = WFWeatherForecast(location: expectedLocation)
        // Assert
        XCTAssertNotNil(sut.location)
    }
    
    func testWeatherForecastShouldHaveDailyForecastsCollection() {
        // Arrange
        let expectedDailyForecasts = [WFDailyForecast()]
        let sut = WFWeatherForecast(dailyForecasts: expectedDailyForecasts)
        // Assert
        XCTAssertNotNil(sut.dailyForecasts)
    }
    
    func testWeatherForecastConformsToObjectMapperProtocol() {
        //Arrange
        let sut = WFWeatherForecast()
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "WeatherForecast should conforms to ObjectMapper protocol")
    }
    
    func testInit_WithNilDictionary_ShouldFailInitialization() {
        //Arrange
        let sut = WFWeatherForecast(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "WeatherForecast should be created only with valid json dictionary")
    }
    
    func testInit_WithCityInfoInDictionary_ShouldSetLocationOfWeatherForecast() {
        //Arrange
        let jsonCityDictionary = ["id":123, "name":"London", "country":"GB"] as [String : Any]
        let jsonWeatherForecastDictionary = ["city":jsonCityDictionary]
        let sut = WFWeatherForecast(dictionary: jsonWeatherForecastDictionary)
        
        //Assert
        XCTAssertNotNil(sut?.location)
        XCTAssertEqual(sut?.location?.id, 123)
        XCTAssertEqual(sut?.location?.name, "London")
        XCTAssertEqual(sut?.location?.country, "GB")
    }
    
    func testInit_WithOneForecastInDictionary_ShouldAddOneDailyForecastIntoForecastsCollectionOfWeatherForecast() {
        //Arrange
        let timeInterval: Double = 1489748400
        let expectedDate = Date(timeIntervalSince1970: timeInterval)
        let jsonForecastDictionary = ["dt": timeInterval]
        let jsonForecastsArray = [jsonForecastDictionary]
        let jsonWeatherForecastDictionary = ["list":jsonForecastsArray]
        let sut = WFWeatherForecast(dictionary: jsonWeatherForecastDictionary)
        
        //Assert
        XCTAssertNotNil(sut?.dailyForecasts)
        XCTAssertEqual(sut?.dailyForecasts?.first?.hourlyForecasts?.first?.date!, expectedDate)
    }
    
    func testInit_WithTwoForecastsInDictionary_ShouldAddTwoDailyForecastIntoForecastsCollectionOfWeatherForecast() {
        //Arrange
        let timeIntervalOfForecastOne: Double = 1489748400
        let expectedDateOfForecastOne = Date(timeIntervalSince1970: timeIntervalOfForecastOne)
        let jsonForecastOneDictionary = ["dt": timeIntervalOfForecastOne]
        let timeIntervalOfForecastTwo: Double = 1492959600
        let expectedDateOfForecastTwo = Date(timeIntervalSince1970: timeIntervalOfForecastTwo)
        let jsonForecastTwoDictionary = ["dt": timeIntervalOfForecastTwo]
        
        let jsonForecastsArray = [jsonForecastOneDictionary, jsonForecastTwoDictionary]
        let jsonWeatherForecastDictionary = ["list":jsonForecastsArray]
        let sut = WFWeatherForecast(dictionary: jsonWeatherForecastDictionary)
        
        //Assert
        XCTAssertNotNil(sut?.dailyForecasts)
        XCTAssertEqual(sut?.dailyForecasts?.first?.hourlyForecasts?.first?.date!, expectedDateOfForecastOne)
        XCTAssertEqual(sut?.dailyForecasts?.last?.hourlyForecasts?.first?.date!, expectedDateOfForecastTwo)
    }
}
