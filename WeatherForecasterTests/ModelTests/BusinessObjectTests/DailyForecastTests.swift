//
//  DailyForecastTests.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 23/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WeatherForecaster

class DailyForecastTests: XCTestCase {
    func testConformanceToDailyForecastProtocol() {
        // Arrange
        let sut = WFDailyForecast()
        // Assert
        XCTAssert((sut as Any) is DailyForecast)
    }
    
    func testDailyForecastShouldHaveADate() {
        // Arrange
        let date = Date()
        let sut = WFDailyForecast(date: date, hourlyForecasts: nil)
        // Assert
        XCTAssert(sut.date == date)
    }
    
    func testDailyForecastShouldHaveHourlyForecastCollection() {
        // Arrange
        let hourlyForecasts = [WFHourlyForecast()]
        let sut = WFDailyForecast(date: nil, hourlyForecasts: hourlyForecasts)
        // Assert
        XCTAssertNotNil(sut.hourlyForecasts)
    }
}
