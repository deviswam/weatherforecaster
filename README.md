PLEASE READ ALL
===============

1. iOS 9 onwards supported. Tested on iPhone 5 onwards devices in both orientation. 
2. The project has been developed on latest xcode 8.3.2 without any warnings or errors.
3. Attached is the test coverage snapshot. Project Test coverage is reported as 86.82% by Xcode.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
5. TDD approach has been adopted for development with XCTest.
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
7. With more time, following things could be implemented/improved:
	* Aiming to achieve 100% test coverage by writing test cases for Error Handling on WeatherManager, WeatherAPI and WeatherForecastViewModel classes. I was aiming to complete the happy path first. Despite of that application code is stable and doesn't crash on httpError and serialization scenarios for example.
	* Presenting the spinner to the user while 5 day weather forecast is being fetched.
	* Test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
	* There are no functional tests written because of limited time. I wish i could have the chance to write them using KIF or iOS UITesting. I've written them in past.
8. Whereever possible i've tried to follow SOLID principles, composition over inheritance and protocol oriented programming.

Thanks for your time.