//
//  ForecastTableViewDataSource.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 24/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ForecastTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: WeatherForecastViewModel?
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = viewModel else { return  nil }
        return viewModel.titleOfDailyForecast(at: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else { return  0 }
        return viewModel.numberOfDailyForecasts()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.numberOfHourlyForecasts(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HourlyForecastTableViewCell", for: indexPath)
        if let hourlyForecastCell = cell as? HourlyForecastTableViewCell,
            let hourlyForecast = viewModel?.hourlyForecast(at: indexPath.section, hourlyForecastIndex: indexPath.row) {
            hourlyForecastCell.configCell(with: hourlyForecast)
        }
        return cell
    }
}
