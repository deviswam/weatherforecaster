//
//  AppCoordinator.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 27/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator {
    private var window: UIWindow
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showWeatherForecast(rootNavController: rootNavController)
        }
    }
    
    private func showWeatherForecast(rootNavController: UINavigationController) {
        if let weatherForecastVC = rootNavController.topViewController as? WeatherForecastVC {
            let weatherAPI = WFWeatherAPI()
            let weatherManager = WFWeatherManager(weatherAPI: weatherAPI)
            let forecastTableViewdataSource = ForecastTableViewDataSource()
            let viewModel = WFWeatherForecastViewModel(weatherManager: weatherManager, viewDelegate: weatherForecastVC)
            
            weatherForecastVC.viewModel = viewModel
            weatherForecastVC.forecastTableViewDataSource = forecastTableViewdataSource
            forecastTableViewdataSource.viewModel = viewModel
        }
    }
}
