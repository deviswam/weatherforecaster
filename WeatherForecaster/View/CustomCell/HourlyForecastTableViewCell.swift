//
//  HourlyForecastTableViewCell.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 26/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol HourlyForecastTableViewCell {
    func configCell(with hourlyForecast: HourlyForecastViewModel)
}

class WFHourlyForecastTableViewCell: UITableViewCell, HourlyForecastTableViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    
    func configCell(with hourlyForecast: HourlyForecastViewModel) {
        hourLabel.text = hourlyForecast.hour
        conditionLabel.text = hourlyForecast.condition
        tempLabel.text = hourlyForecast.temp
        minTempLabel.text = hourlyForecast.minTemp
        maxTempLabel.text = hourlyForecast.maxTemp
    }
}
