//
//  WeatherAPI.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum WeatherAPIError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

protocol WeatherAPI {
    func fiveDaysWeatherForecast(for locationId: Int, completionHandler: @escaping (_ weatherForecast: WeatherForecast?, _ error: WeatherAPIError?) -> Void)
}

enum WeatherAPIMethodType: String {
    case fiveDaysForecast = "forecast"
}

class WFWeatherAPI: WeatherAPI {
    
    let API_KEY = "4b735ecc95c764e35c9842d5efb5de55"
    let BASE_URL = "http://api.openweathermap.org/data/2.5/"
    
    private let urlSession: URLSession!
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func fiveDaysWeatherForecast(for locationId: Int, completionHandler: @escaping (_ weatherForecast: WeatherForecast?, _ error: WeatherAPIError?) -> Void) {
        
        let requestParams = ["id": String(locationId),
                             "units": "metric",
                             "appid":API_KEY]
        guard let urlString = self.createURLString(with: .fiveDaysForecast, parameters: requestParams),
            let url = URL(string: urlString) else {
                return
        }
        print("WAM URL:\(urlString)")
        getWeatherForecast(with: url) { (weatherForecast: WeatherForecast?, error: WeatherAPIError?) in
            completionHandler(weatherForecast, error)
        }
    }
    
    private func createURLString(with apiMethodType:WeatherAPIMethodType, parameters:[String: String]) -> String? {
        var urlComponents = URLComponents(string: BASE_URL + apiMethodType.rawValue)
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        return urlComponents?.url?.absoluteString
    }
    
    private func getWeatherForecast(with url: URL, completionHandler: @escaping (WeatherForecast?, WeatherAPIError?) -> Void) {
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            var weatherForecast : WeatherForecast?
            var retError : WeatherAPIError?
            
            if error != nil {
                retError = WeatherAPIError.httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let _ = dictionary["list"] as? [[String : AnyObject]], let _ = dictionary["city"] as? [String : AnyObject] else {
                            retError = WeatherAPIError.invalidDataError
                            DispatchQueue.main.async {
                                completionHandler(weatherForecast, retError)
                            }
                            return
                        }
                        weatherForecast = WFWeatherForecast(dictionary: dictionary)
                    }
                } catch {
                    retError = WeatherAPIError.dataSerializationError
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(weatherForecast, retError)
            }
        }
        dataTask.resume()
    }
}
