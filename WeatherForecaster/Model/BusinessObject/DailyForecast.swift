//
//  DailyForecast.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol DailyForecast {
    var date: Date? { get }
    var hourlyForecasts: [HourlyForecast]? { get }
}

struct WFDailyForecast: DailyForecast {
    var date: Date?
    var hourlyForecasts: [HourlyForecast]?
    
    init(date: Date? = nil, hourlyForecasts: [HourlyForecast]? = nil) {
        self.date = date
        self.hourlyForecasts = hourlyForecasts
    }
}
