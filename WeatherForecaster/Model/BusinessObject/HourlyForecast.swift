//
//  HourlyForecast.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol HourlyForecast {
    var date: Date? { get }
    var temperature: Int? { get }
    var minTemperature: Int? { get }
    var maxTemperature: Int? { get }
    var condition: String? { get }
}

struct WFHourlyForecast: HourlyForecast, ObjectMapper {
    var date: Date?
    var temperature: Int?
    var minTemperature: Int?
    var maxTemperature: Int?
    var condition: String?
    init(date: Date? = nil, temp: Double? = nil, minTemp: Double? = nil, maxTemp: Double? = nil, condition: String? = nil) {
        self.date = date
        if let temp = temp {
            self.temperature = Int(round(temp))
        }
        if let minTemp = minTemp {
            self.minTemperature = Int(round(minTemp))
        }
        if let maxTemp = maxTemp {
            self.maxTemperature = Int(round(maxTemp))
        }
        self.condition = condition
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary else {
            return nil
        }
        
        var date: Date?
        if let dateUnix = dictionary["dt"] as? Double {
            date = Date(timeIntervalSince1970: dateUnix)
        }
        
        var dTemp, dMinTemp, dMaxTemp: Double?
        if let mainDictionary = dictionary["main"] as? Dictionary<String, Any> {
            dTemp = mainDictionary["temp"] as? Double
            dMinTemp = mainDictionary["temp_min"] as? Double
            dMaxTemp = mainDictionary["temp_max"] as? Double
        }
        
        var dCondition: String?
        if let weatherArray = dictionary["weather"] as? Array<Dictionary<String, Any>>,
            let weatherDictionary = weatherArray.first {
                dCondition = weatherDictionary["main"] as? String
            }
        
        
        self.init(date: date, temp: dTemp, minTemp: dMinTemp, maxTemp: dMaxTemp, condition: dCondition)
    }
}
