//
//  Location.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Location {
    var id: Int { get }
    var name: String? { get }
    var country: String? { get }
}

struct WFLocation: Location, ObjectMapper {
    var id: Int
    var name: String?
    var country: String?
    init(id: Int, name: String? = nil, country: String? = nil) {
        self.id = id
        self.name = name
        self.country = country
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
            let dId = dictionary["id"] as? Int else {
                return nil
        }
        let dName = dictionary["name"] as? String
        let dCountry = dictionary["country"] as? String

        self.init(id: dId, name:dName, country: dCountry)
    }
}

func == (lhs: Location, rhs: Location) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}

