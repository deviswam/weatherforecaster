//
//  WeatherForecast.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol WeatherForecast {
    var dailyForecasts: [DailyForecast]? { get }
    var location: Location? { get }
}

struct WFWeatherForecast: WeatherForecast, ObjectMapper {
    var dailyForecasts: [DailyForecast]?
    var location: Location?
    
    init(dailyForecasts: [DailyForecast]? = nil, location: Location? = nil) {
        self.dailyForecasts = dailyForecasts
        self.location = location
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary else {
                return nil
        }
        
        var dLocation: Location?
        if let locationDictionary = dictionary["city"] as? Dictionary<String, Any> {
            dLocation = WFLocation(dictionary: locationDictionary)
        }
        
        var dDailyForecasts: [DailyForecast]?
        if let forecastsArray = dictionary["list"] as? [[String: Any]] {
            let hourlyForecasts = forecastsArray.map({ (hourlyForecastDictionary) -> HourlyForecast? in
                 return WFHourlyForecast(dictionary: hourlyForecastDictionary)
            }).filter({ (hourlyForecast) -> Bool in
                return hourlyForecast != nil
            }).map { return $0!}
            
            var dailyForecastsDictionary = [Date: [HourlyForecast]]()
            for hourlyForecast in hourlyForecasts {
                if let forecastDate = hourlyForecast.date?.dateWithoutTime() {
                    if dailyForecastsDictionary[forecastDate] != nil {
                        dailyForecastsDictionary[forecastDate]!.append(hourlyForecast)
                    } else {
                        dailyForecastsDictionary[forecastDate] = [hourlyForecast]
                    }
                }
            }
            
            dDailyForecasts = dailyForecastsDictionary.map({ (dailyForecastDate: Date, hourlyForecastsOfADay: [HourlyForecast]) -> DailyForecast in
                return WFDailyForecast(date: dailyForecastDate, hourlyForecasts: hourlyForecastsOfADay)
            }).sorted(by: { (lhsForecast, rhsForecast) -> Bool in
                if let lhsDate = lhsForecast.date, let rhsDate = rhsForecast.date {
                    return lhsDate < rhsDate
                }
                return false
            })
        }
        self.init(dailyForecasts: dDailyForecasts, location: dLocation)
    }
}
