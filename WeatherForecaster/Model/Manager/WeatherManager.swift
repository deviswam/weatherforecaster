//
//  WeatherManager.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum WeatherManagerError : Error {
    case noForecastFoundError
    case connectionError
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noForecastFoundError:
            return "No Forecast Found"
        case .connectionError:
            return "Connection Error"
        }
    }
}

protocol WeatherManager {
    func weatherForecast(for locationId: Int, completionHandler: @escaping (_ weatherForecast: WeatherForecast?, _ error: WeatherManagerError?) -> Void)
}

class WFWeatherManager: WeatherManager {
    private var weatherAPI: WeatherAPI
    init(weatherAPI: WeatherAPI) {
        self.weatherAPI = weatherAPI
    }
    
    func weatherForecast(for locationId: Int, completionHandler: @escaping (_ weatherForecast: WeatherForecast?, _ error: WeatherManagerError?) -> Void) {
        weatherAPI.fiveDaysWeatherForecast(for: locationId) { (weatherForecast: WeatherForecast?, error: WeatherAPIError?) in
            var managerError: WeatherManagerError?
            if let error = error {
                managerError = self.weatherManagerError(from: error)
            }
            completionHandler(weatherForecast, managerError)
        }
    }
    
    private func weatherManagerError(from weatherAPIError: WeatherAPIError) -> WeatherManagerError? {
        switch weatherAPIError {
        case .dataSerializationError, .invalidDataError:
            return WeatherManagerError.noForecastFoundError
        case .httpError:
            return WeatherManagerError.connectionError
        }
    }
}
