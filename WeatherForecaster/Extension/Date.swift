//
//  Date.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 23/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

extension Date {
    var hour: String {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let hr = calendar.component(.hour, from: self)
        return String(hr)
    }
    
    func dateWithoutTime() -> Date? {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: components)
    }
    
    func dayOfWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
