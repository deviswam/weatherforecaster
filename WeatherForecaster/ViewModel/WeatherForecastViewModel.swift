//
//  WeatherForecastViewModel.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum WeatherForecastViewModelResult : Equatable{
    case success
    case failure
}

protocol WeatherForecastViewModelViewDelegate {
     func weatherForecastLoaded(with result: WeatherForecastViewModelResult)
}

protocol WeatherForecastViewModel {
    // getter accessors
    func numberOfDailyForecasts() -> Int
    func titleOfDailyForecast(at dailyForecastIndex: Int) -> String
    func numberOfHourlyForecasts(at dailyForecastIndex: Int) -> Int
    func hourlyForecast(at dailyForecastIndex: Int, hourlyForecastIndex: Int) -> HourlyForecastViewModel?
    func locationName() -> String
    
    // called methods
    func loadWeatherForecast(for locationId: Int)
}

class WFWeatherForecastViewModel: WeatherForecastViewModel {
    private var weatherManager: WeatherManager
    private var viewDelegate: WeatherForecastViewModelViewDelegate
    private var dailyForecasts: [DailyForecast]?
    private var location: String?
    
    init(weatherManager: WeatherManager, viewDelegate: WeatherForecastViewModelViewDelegate) {
        self.weatherManager = weatherManager
        self.viewDelegate = viewDelegate
    }
    
    func loadWeatherForecast(for locationId: Int) {
        var result: WeatherForecastViewModelResult = .failure
        weatherManager.weatherForecast(for: locationId) { [unowned self] (weatherForecast: WeatherForecast?, error: WeatherManagerError?) in
            if let weatherforecast = weatherForecast, error == nil {
                result = .success
                self.dailyForecasts = weatherforecast.dailyForecasts
                self.location = weatherforecast.location?.name
            }
            self.viewDelegate.weatherForecastLoaded(with: result)
        }
    }
    
    func numberOfDailyForecasts() -> Int {
        guard let dailyForecasts = dailyForecasts else { return 0 }
        return dailyForecasts.count
    }
    
    func titleOfDailyForecast(at dailyForecastIndex: Int) -> String {
        guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
              let date = dailyForecasts[dailyForecastIndex].date else {
            return ""
        }
        return date.dayOfWeek()
    }
    
    func numberOfHourlyForecasts(at dailyForecastIndex: Int) -> Int {
        guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
              let hourlyForecasts = dailyForecasts[dailyForecastIndex].hourlyForecasts else {
                return 0
        }
        return hourlyForecasts.count
    }
    
    func hourlyForecast(at dailyForecastIndex: Int, hourlyForecastIndex: Int) -> HourlyForecastViewModel? {
        guard let dailyForecasts = dailyForecasts, dailyForecastIndex < dailyForecasts.count,
            let hourlyForecasts = dailyForecasts[dailyForecastIndex].hourlyForecasts, hourlyForecastIndex < hourlyForecasts.count else {
                return nil
        }
        let hourlyForecast = hourlyForecasts[hourlyForecastIndex]
        return WFHourlyForecastViewModel(hourlyForecast: hourlyForecast)
    }
    
    func locationName() -> String {
        guard let location = location else { return " "}
        return location
    }
}
