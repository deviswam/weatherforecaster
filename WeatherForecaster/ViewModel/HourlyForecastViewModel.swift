//
//  HourlyForecastViewModel.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 25/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol HourlyForecastViewModel {
    var hour: String { get }
    var temp: String { get }
    var minTemp: String { get }
    var maxTemp: String { get }
    var condition: String { get }
}

class WFHourlyForecastViewModel: HourlyForecastViewModel {
    var hour: String = ""
    var temp: String = ""
    var minTemp: String = ""
    var maxTemp: String = ""
    var condition: String = ""
    
    init(hourlyForecast: HourlyForecast) {
        if let date = hourlyForecast.date {
            self.hour = date.hour
        }
        if let temp = hourlyForecast.temperature {
            self.temp = "\(temp)°C"
        }
        if let minTemp = hourlyForecast.minTemperature {
            self.minTemp = String(minTemp)
        }
        if let maxTemp = hourlyForecast.maxTemperature {
            self.maxTemp = String(maxTemp)
        }
        if let condition = hourlyForecast.condition {
            self.condition = condition
        }
    }
}
