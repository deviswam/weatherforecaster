//
//  WeatherForecastVC.swift
//  WeatherForecaster
//
//  Created by Waheed Malik on 22/04/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WeatherForecastVC: UIViewController {

    let LOCATION_ID = 2643743 /* hard coded location id of LONDON */
    
    @IBOutlet weak var forecastTableView: UITableView!
    @IBOutlet weak var locationNameLabel: UILabel!
    
    
    var forecastTableViewDataSource: UITableViewDataSource?
    var viewModel: WeatherForecastViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.forecastTableView.dataSource = forecastTableViewDataSource
        self.loadWeatherForecast(for: LOCATION_ID)
    }
    
    private func loadWeatherForecast(for locationId: Int) {
        guard let viewModel = viewModel else {
            return
        }
        viewModel.loadWeatherForecast(for: locationId)
    }
}

extension WeatherForecastVC: WeatherForecastViewModelViewDelegate {
    func weatherForecastLoaded(with result: WeatherForecastViewModelResult) {
        switch result {
        case .success:
            self.locationNameLabel.text = viewModel?.locationName()
            forecastTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch Weather Forecast")
        }
    }
}
